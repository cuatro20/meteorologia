﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pablo.DataTransferObjects
{
    public class MedicionDTO
    {
        public int id { get; set; }
        public int dia { get; set; }
        public string estacion { get; set; }
        public float temperatura { get; set; }
        public float humedad { get; set; }
    }
}