﻿using Pablo.DataTransferObjects;
using Pablo.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Pablo.AccesoDatos
{
    public class AD_Mediciones
    {
        public static List<Estacion> ObtenerListaEstacion()
        {
            List<Estacion> resultado = new List<Estacion>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from Estaciones";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Estacion aux = new Estacion();
                        aux.id = int.Parse(dr["IdEstacion"].ToString());
                        aux.nombre = dr["Nombre"].ToString();

                        resultado.Add(aux);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }
        public static bool InsertarNuevaVenta(Medicion med)
        {
            bool resultado = false;

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "insert into Mediciones values(@Dia, @IdEstacion, @Temperatura, @Humedad)";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Dia", med.dia); //asigno parametros al command
                cmd.Parameters.AddWithValue("@IdEstacion", med.idEstacion);
                cmd.Parameters.AddWithValue("@Temperatura", med.temperatura);
                cmd.Parameters.AddWithValue("@Humedad", med.humedad);

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta
                cmd.ExecuteNonQuery(); //ejecuta la sentencia
                resultado = true; // si no hubo error es true
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<MedicionDTO> ObtenerListaMediciones()
        {
            List<MedicionDTO> resultado = new List<MedicionDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select m.Id, m.Dia, e.Nombre as Estacion, m.Temperatura, m.Humedad  
	                                    from Mediciones m
	                                    join Estaciones e on e.IdEstacion = m.IdEstacion";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        MedicionDTO aux = new MedicionDTO();
                        aux.id = dr.GetInt32(0);
                        aux.dia = dr.GetInt32(1);
                        aux.estacion = dr["Estacion"].ToString();
                        aux.temperatura = int.Parse(dr["Temperatura"].ToString());
                        aux.humedad = int.Parse(dr["Humedad"].ToString());

                        resultado.Add(aux);
                    }
                }
                dr.Close();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }
    }
}