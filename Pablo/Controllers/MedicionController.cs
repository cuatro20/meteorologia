﻿using Pablo.AccesoDatos;
using Pablo.DataTransferObjects;
using Pablo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pablo.Controllers
{
    public class MedicionController : Controller
    {
        // GET: Persona
        public ActionResult AltaMedicion()
        {
            MedicionVM vm = new MedicionVM();
            vm.estacion = AD_Mediciones.ObtenerListaEstacion();

            return View(vm);
        }
        // para hacer el post de la nueva persona y ver la lista
        [HttpPost]
        public ActionResult AltaMedicion(MedicionVM model)
        {
            if (ModelState.IsValid)
            {
                bool resultado = AD_Mediciones.InsertarNuevaVenta(model.medicionModel);
                if (resultado)
                {
                    return RedirectToAction("ListadoMedicion", "Medicion");
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                model.estacion = AD_Mediciones.ObtenerListaEstacion();
                return View(model);
            }
        }
        // get de la lista
        public ActionResult ListadoMedicion()
        {
            List<MedicionDTO> lista = AD_Mediciones.ObtenerListaMediciones();
            return View(lista);
        }
    }
}