﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pablo.Models
{
//Id int identity PK

//Dia int

//IdEstacion int FK

//Temperatura float

//Humedad float
    public class Medicion
    {
        public int id { get; set; }
        public int dia { get; set; }
        public int idEstacion { get; set; }
        public float temperatura { get; set; }
        public float humedad { get; set; }
    }
}